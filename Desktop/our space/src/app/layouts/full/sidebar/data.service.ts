import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChildrenOutletContexts } from '@angular/router';
import { map, Observable } from 'rxjs';
import { Data } from './data';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  x:string ="hehe";

  constructor(public http: HttpClient) { }

  getGroupsEndpoint(id: string): string
  {
    return `https://gitlab.com/api/v4/groups/${id}/subgroups?${environment.privateToken}`
    
  }
  getProjectEndpoint(id: string): string
  {
    return `https://gitlab.com/api/v4/groups/${id}/projects?${environment.privateToken}`
  }
  getProjectsEndpoint(id: string): string
  {
    return `https://gitlab.com/api/v4/projects/${id}?${environment.privateToken}`
  }
	
	getGroupEndpoint(id: string): string
  {
    return `https://gitlab.com/api/v4/groups/${id}?${environment.privateToken}`
  }
   getRoot(): Observable<Data[]>{
    
    return this.http.get<Data[]>(this.getGroupsEndpoint(environment.groupID)).pipe(
      map((res: any) =>
        res.map((data: Data) =>
        {
          return {
            name: data.name,
            id: data.id,
            
           
          }
        })
      )
    )
    }

  getNodes(subid: string): Observable<Data[]>
  {
    return this.http.get<Data[]>(this.getGroupsEndpoint(subid)).pipe(
      map((res: any) =>
        res.map((data: Data) =>
        {
          return {
            name: data.name,
            id: data.id,
            children: data.children

           
          }
        })
      )
    )
  } 
  
  
  
}
