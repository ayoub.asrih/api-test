import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-accesses',
  templateUrl: './accesses.component.html',
  styleUrls: ['./accesses.component.css']
})
export class AccessesComponent implements OnInit {


  catList:any =[];
  envList:any =[];
  tagList:any =[];
  checkedd = false;
  filteredArray:any =[];
  loadingIndicator = true;
	reorderable = true;
  ColumnMode = ColumnMode;
  dataRows:any =[];
  accessText:any;
  input:any="";
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getData();
    this.filter("");
  }
  
  getData()
	{
		this.http.get("assets/json/config.json").subscribe((data:any) =>{
      /* this.accessText = data.content;
			this.accessText = atob(this.accessText);
			console.log(this.accessText);
			this.accessText = JSON.parse(this.accessText); 
      this.dataRows = this.accessText; */
       this.filteredArray = data;    
       this.dataRows = data;
	})
  
	}
  catChange(option:any, event:any) {
    if(event.checked) {
      this.catList.push(option);
    } else {
    for(var i=0 ; i < this.dataRows.length; i++) {
      if(this.catList[i] == option) {
        this.catList.splice(i,1);
     }
   }
 }
 console.log(this.catList);
}
  envChange(option:any, event:any) {
    if(event.checked) {
     
      this.envList.push(option);
    } else {
    for(var i=0 ; i < this.dataRows.length; i++) {
      if(this.envList[i] == option) {
        this.envList.splice(i,1);
     }
   }
 }
 console.log(this.envList);
}
  tagChange(option:any, event:any) {
    if(event.checked) {
      console.log(event)
      this.tagList.push(option);
    } else {
    for(var i=0 ; i < this.dataRows.length; i++) {
      if(this.tagList[i] == option) {
        this.tagList.splice(i,1);
     }
   }
 }
 console.log(this.tagList);
}
  Reset()
  {
    this.ngOnInit();
    this.filteredArray = this.dataRows;
    
    
  }

  filter(value:any)
  {
    console.log(value)
    
    if (value != "")
    {
      console.log(value);
      this.filteredArray = this.dataRows.filter((row:any)=>{
        if((value === row.username) == true)
          return value === row.username
        else if((value === row.url) == true)
          return value === row.url
        else if((value === row.password) == true)
          return value === row.password
      })
    }
    else if(this.catList.length == 0 && this.envList.length == 0 && this.tagList.length == 0)
    {
      this.filteredArray = this.dataRows;
    }
    else
    {
      this.filteredArray = this.dataRows.filter((row:any)=>{
        let inc1 = this.catList.includes(row.category)
        let inc2 = this.envList.includes(row.environnement)
        let inc3 = this.tagList.includes(row.tag)
        if(this.catList.length>0 && this.envList.length>0 && this.tagList.length>0)
        {
          if(inc1 == true && inc2 == true && inc3 == true)
            return true
          else 
            return false
        }
        else if(this.catList.length>0 && this.envList.length>0 && this.tagList.length==0)
        {
          if(inc1 == true && inc2 == true && inc3 == false)
            return true
          else 
            return false
        }
        else if(this.catList.length>0 && this.envList.length==0 && this.tagList.length>0)
        {
          if(inc1 == true && inc2 == false && inc3 == true)
            return true
          else 
            return false
        }
        else if(this.catList.length==0 && this.envList.length>0 && this.tagList.length>0)
        {
          if(inc1 == false && inc2 == true && inc3 == true)
            return true
          else 
            return false
        }
        else
          return inc1+inc2+inc3;
        
      })
    }

    /* if(this.catList.length > 0)
    { 
        this.filteredArray = this.dataRows.filter((row:any)=>{
          return this.catList.includes(row.category);
        })
    }

    if(this.envList.length > 0)
    { 
        this.filteredArray = this.dataRows.filter((row:any)=>{
          return this.envList.includes(row.environnement);
        })
    }

    if(this.tagList.length > 0)
    { 
        this.filteredArray = this.dataRows.filter((row:any)=>{
          return this.tagList.includes(row.tag);
        })
    } */
    
  }

}
