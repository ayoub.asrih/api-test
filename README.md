# Conception et Dévloppement d'un Portail Développeur 

Ce portail Web permet aux développeurs d'accéder à plusieurs données de nombreux projets et sous-groupes dans un groupe GitLab privé de l'entreprise

## Pour commencer

Cloner le projet en local :
``git clone https://gitlab.com/ayoub.asrih/our-space-ui.git``

Ouvrir le dossier du projet : 
``cd "our space"``

Avoir npm est nécessaire pour démarrer le projet ([télécharger npm](https://nodejs.org/en/download/))

## Démarrage en mode Locale

Lancer la commande ``ng serve`` dans le terminal, puis accéder le lien suivant : `http://localhost:4200/`

## Déployer le projet 

Lancer la commande ``ng build`` dans le terminal, les artifacts du build seront sauvegardé dans le dossier ``dist``

## Les technologies utilisé 

* [Angular](http://angular.io) - TypeScript-based web application framework
* [Node.js](https://nodejs.org/en/about/) - JavaScript runtime environment
* [Material pro](https://demos.wrappixel.com/free-admin-templates/angular/material-angular-free/angular/dashboard) - Angular 13 cli material template
* [Visual Studio Code](http://angular.io) - Éditeur de code

## Variables d'environnement

toutes les variables d'environnement du projet se trouvent dans les fichiers environment.ts(pour l'environnement Dev) et environement.prod.ts(pour l'environnement Prod)

## Générer le token GitLab


1. Connectez-vous à GitHub.
2. Accédez aux paramètres de votre compte GitHub.
3. Faites défiler vers le bas et cliquez sur « Developer settings » dans la liste des liens à gauche.
4. Cliquez sur le lien Jetons d’accès personnels.
5. Cliquez sur le bouton « Générer un nouveau jeton ».
6. Ajoutez une « Note » pour décrire l’utilisation des jetons.
7. Définissez une date d’expiration pour le jeton d’accès personnel GitHub.
8. Sélectionnez la portée d’authentification appropriée.
9. Cliquez sur le bouton « Générer un jeton ».
10. Copiez ensuite votre jeton d’accès personnel avant de quitter la page, car la valeur alphanumérique du jeton ne sera plus affichée.
